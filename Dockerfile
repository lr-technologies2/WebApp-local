FROM traefik:latest as base

ENV NODE_ENV=development

# Install openssl, basically not available on traefik images
RUN apk add --no-cache openssl

## Create self-signed ssl keys for test purposes (bind mount proper ones to running container)
RUN openssl req -new -x509 -nodes -out /home/ssl.cert -keyout /home/ssl.key -days 3650 -subj '/CN=localhost' -addext "subjectAltName = DNS:localhost"

EXPOSE 80 443 3003
