# HOW TO RUN PROJECT ON YOUR LOCALE MACHINE
## Prerequisites
- [Project functioning](https://lrtechnologies78-my.sharepoint.com/:w:/g/personal/libellien15_lrtechnologies_fr/ES26308B2DxBlWXDpke5lw0BNIRm9NLHNWzNGogSmo05OQ?e=8QUE6o)
- [Docker desktop](https://www.docker.com/products/docker-desktop/) installed (available for Mac, Linux and Windows)
- For Windows users, WSL installed
- If not installed yet, install NPM in your WSL distribution (ie apt-get install npm for Debian / Ubuntu)
- Install docker (Debian/ Ubuntu: sudo apt update; sudo apt upgrade; sudo apt install docker.io)
- Create and configure the docker user group:
  - ```Sudo groupadd docker```
  - ```Sudo usermod -aG docker $USER```
  - Logout and login again.

- Clone (`git clone {SSH or HTTPS URL}`) following projects on your machine (Pull dev branches)
  - [This project](https://gitlab.com/lr-technologies2/WebApp-local)
  - [Landing page project](https://gitlab.com/lr-technologies2/webapp)
  - [Back project](https://gitlab.com/lr-technologies2/webapp_back)
  - [Front project](https://gitlab.com/lr-technologies2/lalibellule-app)

## Build images locally
Run following commands on projects:  
- webapp-local: ```docker build . -t routeur```
- lalibelluleapp : ```docker build . -t angular```
- webapp_back : ```docker build . -t api --target local```
- webapp : ```docker build . -t landing```

## Create local network
```docker network create webapp```

## Launch local project
On this project (webapp_local), run the following command to launch all services on different containers on your machine.  
```docker-compose up```

:warning: If the command fails with ``ERROR: The Compose file './docker-compose.yml' is invalid because:
networks.frontend value Additional properties are not allowed``, you are using the wrong version of docker compose. Get the latest version here (see: https://stackoverflow.com/questions/58155523/unable-to-give-network-name-in-docker-compose): 
1. ```curl -L https://github.com/docker/compose/releases/download/1.28.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose```
2. ```chmod +x /usr/local/bin/docker-compose```
3. use: ```usr/local/bin/docker-compose up```

:warning: If docker compose complains about a specific port not being available, try to kill apache :
```sudo /etc/init.d/apache2 stop```

## Activate self signed certificate on Chrome
<strong style="color:orange">This is to be done each time you run `docker-compose up` command, or you should open in private navigation window</strong>
- Go to https://api.localhost
- Click "Advanced" button then "Proceed to ..."

## Enjoy !
:tada: You can now access following interfaces : :tada:
- [Application](https://app.localhost) 
- [Api](https://api.localhost) (should not be used in web-browser but with Postman)
- [PhpMyAdmin](https://pma.localhost) 
 
Credentials are available on [Project functioning doc](https://lrtechnologies78-my.sharepoint.com/:w:/g/personal/libellien15_lrtechnologies_fr/ES26308B2DxBlWXDpke5lw0BNIRm9NLHNWzNGogSmo05OQ?e=8QUE6o)
