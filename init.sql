DROP DATABASE IF EXISTS lr;

CREATE DATABASE lr;

USE lr;

DROP TABLE IF EXISTS user;
--
-- Structure de la table `sensor`
--

CREATE TABLE `sensor` (
  `id` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `id_sensor_type` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sensor_type`
--

CREATE TABLE `sensor_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `password` char(60) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` enum('Admin','Therapist','Patient','Maker','Helper') NOT NULL,
  `birth_date` BIGINT(20) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_verified` int(1) DEFAULT 0,
  `token` varchar(6) NOT NULL COMMENT 'token de vérification',
  `last_login` timestamp DEFAULT NULL,
  `last_password_updated` timestamp DEFAULT NULL,
  `suppression_date` timestamp DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Structure de la table `user_sensor`
--

CREATE TABLE `user_sensor` (
  `id_sensor` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `id_user` int(11) NOT NULL,
  `attribution_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Index pour les tables déchargées
--

--
-- Index pour la table `sensor`
--
ALTER TABLE `sensor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sensor_type` (`id_sensor_type`);

--
-- Index pour la table `sensor_type`
--
ALTER TABLE `sensor_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `user_sensor`
--
ALTER TABLE `user_sensor`
  ADD KEY `id_sensor` (`id_sensor`),
  ADD KEY `id_user` (`id_user`);
  
--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `sensor_type`
--
ALTER TABLE `sensor_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `sensor`
--
ALTER TABLE `sensor`
  ADD CONSTRAINT `sensor_ibfk_1` FOREIGN KEY (`id_sensor_type`) REFERENCES `sensor_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

--
-- Contraintes pour la table `user_sensor`
--
ALTER TABLE `user_sensor`
  ADD CONSTRAINT `user_sensor_ibfk_1` FOREIGN KEY (`id_sensor`) REFERENCES `sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_sensor_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;


INSERT INTO `user` (`id`, `password`, `first_name`, `last_name`, `email`, `role`, `birth_date`, `creation_date`, `user_verified`, `token`) VALUES
(1, '$2a$08$TIdJ/Wfw64mm3xswfr.xZ.SbGLxsBnFxdGZSFM4qr.lJNtiCev7Mm', 'Jean', 'Blaguain', 'libellien15@lrtechnologies.fr', 'Admin', 325987200000, '2022-09-21 14:21:51', 1, 'i65xg'),
(2, '$2a$08$SUtDtWwJYFB0PuFINGzXEeLAgWTrQWtnI2fRI5TJWeJt4T9kaDKCC', 'Camille', 'Onette', 'c.onette@lrtechnologies.fr', 'Therapist', 139791600000, '2022-09-29 13:25:02', 1, 'ztqw7'),
(3, '$2a$08$hO0/JylVr/BKUogpxcnCJOZ66L7.hEr9vGSwobB2T7b494fGBZQMW', 'Richard', 'Dasso', 'r.dasso@lrtechnologies.fr', 'Helper', 753404400000, '2022-09-29 13:25:02', 1, 'uhnn1f'),
(4, '$2a$08$R/CUxQeNg.GPDbkaVLDMteOSAzme24Q5oOAqKbt5gD4c69EMgQQGe', 'Marie', 'Viere', 'm.viere@lrtechnologies.fr', 'Patient', 824338800000, '2022-09-29 13:25:02', 1, '60zqt')
;

INSERT INTO sensor_type (`id`, `name`) VALUES
(1, 'Capteur main'),
(2, 'Capteur visage')
;

INSERT INTO `sensor` (`id`, `name`, `id_sensor_type`) VALUES
('0x0025', 'Capteur main droite', 1),
('0x0026', 'Capteur visage', 2),
('0x0022', 'Capteur main gauche', 1)
;

INSERT INTO `user_sensor` (`id_sensor`, `id_user`, `attribution_date`) VALUES
('0x0025', 4, '2022-09-30 14:59:36.000'),
('0x0026', 4, '2022-09-30 15:01:12.000'),
('0x0022', 4, '2022-10-04 10:10:22.000')
;
